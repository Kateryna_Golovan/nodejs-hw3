const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

const app = express();

const {authRouter} = require('./src/controllers/authController');
const {userRouter} = require('./src/controllers/userController');
const {truckRouter} = require('./src/controllers/truckController');
const {loadRouter} = require('./src/controllers/loadController');
const {NodeCourseError} = require('./src/utils/errors');
const {authMiddleware} =require('./src/middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', [authMiddleware], userRouter);
app.use('/api/trucks', [authMiddleware], truckRouter);
app.use('/api/loads', [authMiddleware], loadRouter);


app.use((req, res, next) => {
  res.status(404).json({message: 'Not found'});
});

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://katrusya:my password 4 MONGODB@katgolcluster.ubmfa.mongodb.net/nodeJShw2?retryWrites=true&w=majority', {
      useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true,
    });
    app.listen(8080, () => console.log('Server working on 8080'));
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};
start();
