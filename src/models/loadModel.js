const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
    default: null,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: {},
    required: true,
  },
  logs: {
    type: Array,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Load};
