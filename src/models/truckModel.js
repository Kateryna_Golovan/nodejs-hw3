const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    default: 'IS',
  },
  type: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Truck};
