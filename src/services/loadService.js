const {Load} = require('../models/loadModel');
const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');

const {BadRequestError} = require('../utils/errors');

const getUserLoads = async (userId, offset, limit=10) => {
  const loads = await Load.find({created_by: userId})
    .skip(offset)
    .limit(limit);

  return loads;
};

const addLoadToUser = async (userId, data) => {
  const load = new Load({...data, userId});

  await load.save();
};


const getUsersActiveLoads = async (userId) => {
  const load = await Load.findOne({
    assigned_to: userId,
  });
  
  return load;
};

const iterateLoadState = async (userId) => {
  const load = await Load.findOne({
    assigned_to: userId,
    state: {$ne: null},
  });
};

const getUserLoadById = async (id, userId) => {
  await Load.findOne({_id: id});
};

const updateUserLoadById = async (id, userId, data) => {
  const load = await Load.findOneAndUpdate({
    _id: id,
    created_by: userId,
    status: 'NEW',
  }, {$set: {data}});

};

const deleteUserLoadById = async (id, userId) => {
   const load = await Load.findOneAndDelete({
    _id: id,
    created_by: userId,
    status: 'NEW',
  });

};


const postUserLoadById = async (id, userId) => {
  const load = await Load.findOneAndUpdate({
    _id: id,
    created_by: userId,
  }, {$set: {status: 'POSTED'}});

  const truck = await Truck.find({
    status: 'IS',
    assigned_to: {$ne: null},
  });

  if (load.payload < truck.payload && load.dimensions.lengt < truck.dimensions.length && load.dimensions.height < truck.dimensions.height){
    load.status = 'ASSIGNED';
    load.state = 'En route to Pick Up';
    load.logs.push({
      message: `Load assigned to driver with id ${truck.assigned_to}`,
      time: new Date(Date.now()),
    });
    truck.status = 'OL';
    await load.save();
    await truck.save();
  }
  if (load.assigned_to) {
    return {
      message: 'Load posted successfully',
      driver_found: true,
    };
  }

  if (!load.assigned_to) {
    return {
      message: 'To truck is awailable',
      driver_found: false,
    };
  }
};

const getUserLoadShippingInfoById = async (id, userId) => {
  const load = await Load.findOne({
    _id: id,
    created_by: userId,
  });
  const truck = await Truck.findOne({
    assigned_to: load.assigned_to,
  });
 
  return {'load': load, 'truck': truck};
};

module.exports = {
  getUserLoads,
  addLoadToUser,
  getUsersActiveLoads,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingInfoById
};

