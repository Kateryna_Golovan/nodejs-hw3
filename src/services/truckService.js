const {Truck} = require('../models/truckModel');
const {InvalidRequestError} = require('../utils/errors');

const getDriverTrucksByDriverId = async (userId) => {
  const trucks = await Truck.find({created_by: userId});
  return trucks;
};

const addTruckToDriver = async (userId, truckPayload) => {
  if (truckPayload.type === 'SPRINTER') {
    truckPayload.payload = 1000;
  }
  if (truckPayload.type === 'SMALL STRAIGHT') {
    truckPayload.payload = 2000;
  }
  if (truckPayload.type === 'LARGE STRAIGHT') {
    truckPayload.payload = 3000;
  }
  const truck = new Truck({...truckPayload, userId});
  await truck.save();
};

const getDriverTruckById = async (id, userId) => {
  const truck = await Truck.findOne({_id: id, created_by: userId});
  return truck;
};

const updateDriverTruckById = async (id, userId, type) => {
  const truck = Truck.findOne({_id: id, created_by: userId});
  await truck.updateOne({$set: {type}});
};

const deleteDriverTruckById = async (id, userId) => {
  const truck = Truck.findOne({_id: id, created_by: userId});
  await truck.remove();
};

const assignTruckToDriverById = async (id, userId) => {
    const truck = await Truck.findOneAndUpdate(
      {_id: id, created_by: userId},
      {$set: {assigned_to: userId}},
  );
};

module.exports = {
  getDriverTrucksByDriverId,
  addTruckToDriver,
  getDriverTruckById,
  updateDriverTruckById,
  deleteDriverTruckById,
  assignTruckToDriverById,
};
