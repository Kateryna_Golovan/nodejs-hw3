const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');
const {InvalidCredentialstError} = require('../utils/errors');

const getUserProfile = async (userId) => {
  const user = await User.findById(userId);
  return {
    user: {
      _id: user._id,
      role: user.role,
      email: user.email,
      created_date: user.createdDate,
    },
  };
};

const deleteUser = async (userId) => {
  await User.findOneAndRemove({_id: userId});
};

const changePassword = async (req, res) => {
  await User.findByIdAndUpdate(userId, {
    password: await bcrypt.hash(newPassword, 10),
});

};

module.exports = {
  getUserProfile,
  deleteUser,
  changePassword
};
