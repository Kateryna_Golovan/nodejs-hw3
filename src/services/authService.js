const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();

const {User} = require('../models/userModel');
const {InvalidRequestError} = require('../utils/errors');

const registration = async ({email, password, role}) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();
};

const login = async ({email, password}) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new InvalidRequestError('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new InvalidRequestError('Invalid email or password');
  }

  const jwt_token = jwt.sign({
    _id: user._id,
    email: user.email,
  }, 'secret');
  return jwt_token;
};

module.exports = {
  registration,
  login
};
