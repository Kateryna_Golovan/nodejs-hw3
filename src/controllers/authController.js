const express = require('express');
const router = new express.Router();

const {
  registration,
  login
} = require('../services/authService');

const {
  asyncAwaitWrapper,
} = require('../utils/apiUtils');
const {
  registrationValidator,
} = require('../middlewares/validationMiddleware');

router.post('/register', registrationValidator, asyncAwaitWrapper(async (req, res) => {
  const {
    email, 
    password, 
    role
  } = req.body;

  await registration({email, password, role});
  res.json({'message': 'Profile created successfully'});
}));



router.post('/login', asyncAwaitWrapper(async (req, res) => {
      const {
        email,
        password
      } = req.body;

      const jwt_token = await login({email, password});
      res.json({jwt_token, message: 'Logged in successfully!'});
    }),
);

module.exports = {
  authRouter: router,
};
