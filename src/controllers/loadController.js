const express = require('express');
const router = new express.Router();

const {User} = require('../models/userModel');
const {InvalidRoleError, InvalidTypeError, InvalidRequestError} = require('../utils/errors')


const {
  getUserLoads,
  addLoadToUser,
  getUsersActiveLoads,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingInfoById
} = require('../services/loadService');

const {asyncAwaitWrapper} = require('../utils/apiUtils');

router.get('/', asyncAwaitWrapper( async (req, res) => {
  const {userId, role} = req.user;
  const status = req.query.status;
  const loads = await getUserLoads(userId, role, status);

  res.json({loads});
})),


router.post('/', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  const data = req.body;
  const {role} = req.user;

  if (role == "DRIVER") {
    throw new InvalidRequestError('Not available for drivers');
  } else {
    await addLoadToUser(userId, data);
    res.json({'message': 'Load created successfully'});
  }
}));

router.get('/active', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const {role} = req.user;
console.log(role);
  if (role == "SHIPPER") {
    throw new InvalidRequestError('Only available for drivers');
  } else {
    const load = await getUsersActiveLoads(userId);
    res.json({load});
  }
}));

router.patch('/active/state', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  const newState = await iterateLoadState(userId);
  res.json({"message": `Load state changed to '${newState}'`});
}));


router.get('/:id', asyncAwaitWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  
  if (!load) {
    throw new InvalidRequestError('No load with such id found!');
  }

  const load = await getUserLoadById(id, userId);
  res.json({load});
}));

router.put('/:id', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const data = req.body;
  const {role} = req.user;

  if (role == "DRIVER") {
    throw new InvalidRequestError('Not available for drivers');
  } else {
    await updateUserLoadById(id, userId, data);
    res.json({"message": 'Load details changed successfully'});
  }
}));


router.delete('/:id', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await deleteUserLoadById(id, userId);

  res.json({"message": 'Load deleted successfully'});
}));


router.post('/:id/post', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const {role} = req.user;

  if (role == "DRIVER") {
    throw new InvalidRequestError('Not available for drivers');
  } else {
    const done = await postUserLoadById(id, userId);
    res.json({
      "message": 'Load posted successfully',
      "driver_found": done,
  });
  }
}));

router.get('/:id/shipping_info', asyncAwaitWrapper( async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const {role} = req.user;

  if (role == "DRIVER") {
    throw new InvalidRequestError('Not available for drivers');
  } else {  
    const shippingInfo = await getUserLoadShippingInfoById(id, userId);
    res.json({shippingInfo});
  }
}));

module.exports = {
  loadRouter: router,
};

