const express = require('express');
const router = new express.Router();

const {
  getDriverTrucksByDriverId,
  addTruckToDriver,
  getDriverTruckById,
  updateDriverTruckById,
  deleteDriverTruckById,
  assignTruckToDriverById,
} = require('../services/truckService');
const {asyncAwaitWrapper} = require('../utils/apiUtils');
const {truckValidator} = require ('../middlewares/validationMiddleware')

router.get('/', asyncAwaitWrapper(async (req, res) => {
      const {userId} = req.user;
      const getDriverTrucks = await getDriverTrucksByDriverId(userId);
      res.json({trucks: getDriverTrucks});
    }),
);

router.post('/', truckValidator, asyncAwaitWrapper(async (req, res) => {
      const {userId} = req.user;
      const truck = {
        type: req.body.type,
        created_by: userId,
      };

      await addTruckToDriver(userId, truck);
      res.json({message: 'Truck created successfully'});
    }),
);

router.get('/:id', asyncAwaitWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;

      const truck = await getDriverTruckById(id, userId);
      res.json({truck});
    }),
);

router.put('/:id', truckValidator, asyncAwaitWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;
      const {type} = req.body;

      await updateDriverTruckById(id, userId, type);
      res.json({message: 'Truck details changed successfully'});
    }),
);

router.delete('/:id', asyncAwaitWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;

      await deleteDriverTruckById(id, userId);
      res.json({message: 'Truck deleted successfully'});
    }),
);

router.post('/:id/assign', asyncAwaitWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;

      await assignTruckToDriverById(id, userId);
      res.json({message: 'Truck assigned successfully'});
    }),
);

module.exports = {
  truckRouter: router,
};
