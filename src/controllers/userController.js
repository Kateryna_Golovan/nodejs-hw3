const express = require('express');
const router = new express.Router();

const {
  getUserProfile,
  deleteUser,
  changePassword
} = require('../services/userService');
const {
  asyncAwaitWrapper
} = require('../utils/apiUtils');
const {
  passwordValidator
} = require('../middlewares/validationMiddleware');

router.get('/', asyncAwaitWrapper(async (req, res) => {
      const {userId} = req.user;

      const user = await getUserProfile(userId);
      res.json(user);
    }),
);

router.delete('/', asyncAwaitWrapper(async (req, res) => {
      const {userId} = req.user;

      await deleteUser(userId);
      res.json({message: 'Profile deleted successfully'});
    }),
);

router.patch('/password', passwordValidator, asyncAwaitWrapper(async (req, res) => {
      const {userId} = req.user;
      const {
        oldPassword,
        newPassword,
      } = req.body;

      await changePassword(userId, oldPassword, newPassword);
      res.json({message: 'Password changed successfully'});
    }),
);

module.exports = {
  userRouter: router,
};
