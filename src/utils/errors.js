class NodeCourseError extends Error {
  constructor (message) {
      super(message);
      this.status = 500;
  }
}

class InvalidRequestError extends NodeCourseError {
  constructor (message = 'Invalid request') {
      super(message);
      this.status = 400;
  }
}

class InvalidCredentialstError extends NodeCourseError {
  constructor(message = 'Invalid credentials') {
    super(message);
    this.status = 401;
  }
}

class InvalidRoleError extends NodeCourseError {
  constructor(message) {
    super(message);
    this.status = 403;
  }
}

class InvalidTypeError extends NodeCourseError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

module.exports = {
  NodeCourseError,
  InvalidRequestError,
  InvalidCredentialstError,
  InvalidRoleError, 
  InvalidTypeError
};
